/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.uc.dei.cms.termextraction.pipe;

import cc.mallet.pipe.Pipe;
import cc.mallet.types.Instance;
import cc.mallet.types.LabelAlphabet;
import java.io.IOException;
import java.io.InputStream;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

/**
 * NOTE: POSTagger pipe most go before the Sentence2TokenSequence pipe in order 
 * to have access to the whole instead of only one token (the whole sentence is
 * needed for the part-of-speech tagging)
 *
 * @author fmpr
 */
public class POSTagger extends Pipe {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private POSTaggerME posTagger = null;
	private String prefix;

    public POSTagger(String prefix) {
        super(null, new LabelAlphabet());

        this.prefix = prefix;
        // Load OpenNLP POSTagger models
        InputStream modelIn = null;
        try {
            modelIn = getClass().getResourceAsStream("/models/opennlp/en-pos-maxent.bin");
            POSModel model = new POSModel(modelIn);
            posTagger = new POSTaggerME(model);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                } catch (IOException e) {
                }
            }
        }
    }

    @Override
    public Instance pipe(Instance carrier) {
        Object inputData = carrier.getData();
        //System.out.println("inputData=" + inputData);
        String newInputData = "";
        if (inputData instanceof String) {
            String[] lines = ((String) inputData).split("\n");
            String[] tokens = new String[lines.length];
            String[] labels = new String[lines.length];
            for(int i = 0; i<lines.length; i++) {
                String[] splitedLine = lines[i].split("\\s+");
                tokens[i] = splitedLine[0];
                labels[i] = splitedLine[1];
            }
            // Use OpenNLP to find the POS tags
            String[] posTags = posTagger.tag(tokens);
            for(int i = 0; i<lines.length; i++) {
                lines[i] = tokens[i] + " " + prefix + posTags[i] + " " + labels[i];
                newInputData += lines[i] + "\n";
            }
        }
        //System.out.println("newInputData=" + newInputData);
        carrier.setData(newInputData);
        return carrier;
    }

    public POSTaggerME getPosTagger() {
        return posTagger;
    }

    public void setPosTagger(POSTaggerME posTagger) {
        this.posTagger = posTagger;
    }
}
