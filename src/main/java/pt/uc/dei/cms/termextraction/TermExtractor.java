package pt.uc.dei.cms.termextraction;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pt.uc.dei.cms.termextraction.evaluator.MultiSegmentationEvaluator;
import pt.uc.dei.cms.termextraction.evaluator.PerClassAccuracyEvaluator;
import pt.uc.dei.cms.termextraction.pipe.POSTagger;

import cc.mallet.fst.CRF;
import cc.mallet.fst.CRFTrainerByThreadedLabelLikelihood;
import cc.mallet.fst.Transducer;
import cc.mallet.fst.TransducerEvaluator;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.SimpleTaggerSentence2TokenSequence;
import cc.mallet.pipe.TokenSequence2FeatureVectorSequence;
import cc.mallet.pipe.TokenSequenceLowercase;
import cc.mallet.pipe.iterator.LineGroupIterator;
import cc.mallet.pipe.tsf.OffsetConjunctions;
import cc.mallet.pipe.tsf.RegexMatches;
import cc.mallet.pipe.tsf.TokenFirstPosition;
import cc.mallet.pipe.tsf.TokenText;
import cc.mallet.types.InstanceList;
import cc.mallet.types.Sequence;

public class TermExtractor {
	
	public static final int DEFAULT_NUM_THREADS = 4;
	public static final double DEFAULT_GUASSIAN_VARIANCE = 10.0;
	public static final double DEFAULT_TRAINING_FRACTION = 0.9;
	public static final int DEFAULT_RANDOM_SEED = 0;
	

	public static final int DEFAULT_EVAL_PERIOD = 5;
	public static final int DEFAULT_NFOLDS = 0;
	public static final String DEFAULT_LABEL = "O";
	public static final String[] START_TAGS = {"I-KEYWORD",};
	public static final String[] CONTINUE_TAGS = {"C-KEYWORD",};
	public static final int DEFAULT_MODEL_ORDER = 1;
	public static final String FORBIDDEN_PATTERN = "O,C-*";
	public static final int DEFAULT_ITERATIONS = Integer.MAX_VALUE;
	
	private Logger logger = LoggerFactory.getLogger(TermExtractor.class);
	
	private CRF crfmodel;
	private SerialPipes featurePipeline;
	
	private double trainingFraction;
	private double gaussianVariance;
	private int randomSeed;
	private int iterations;
	private int evalPeriod;
	private int [] modelOrder;
	private int nfolds;
	
	public TermExtractor() {
		
		this.trainingFraction = DEFAULT_TRAINING_FRACTION;
		this.gaussianVariance = DEFAULT_GUASSIAN_VARIANCE;
		this.randomSeed = DEFAULT_RANDOM_SEED;
		this.iterations = DEFAULT_ITERATIONS;
		this.evalPeriod = DEFAULT_EVAL_PERIOD;
		this.modelOrder = new int[] {DEFAULT_MODEL_ORDER};
		this.nfolds = DEFAULT_NFOLDS;
		
		this.initilizePipes();
	}
	
	public TermExtractor(CRF crf) {
		this();
		this.crfmodel = crf;
	}

	
	/*
	 * @param filename Model filename
	 * 
	 * Read the TermExtractor model from a file
	 * 
	 * @return A TermExtractor model
	 */
	public static TermExtractor loadModel(String filename) throws Exception {
		TermExtractor result;
		
		ObjectInputStream s =
            new ObjectInputStream(new FileInputStream(filename));
		
		CRF crf = (CRF) s.readObject();
		s.close();
		
		result = new TermExtractor(crf);
		return result;
	}
	
	public void trainModel(String trainDataFilename) throws IOException {
		trainModel(trainDataFilename, DEFAULT_NUM_THREADS);
	}
	
	public void trainModel(String trainDataFilename, int nthreads) throws IOException {
		InstanceList trainData, testData;
		TransducerEvaluator [] evals;
		
		// The training file contains tags (aka targets)
		featurePipeline.setTargetProcessing(true);
		// read all instances from trainning file
		InstanceList instances = loadInstances(trainDataFilename);
        
        /* split instances into training and testing data
         * to prevent overfitting
         */
		
        Random r = new Random(randomSeed);
        InstanceList[] trainingLists =
        	instances.split(r, new double[]{trainingFraction,
            1 - trainingFraction});
        trainData = trainingLists[0];
        testData = trainingLists[1];
        
        if (nfolds==0) {
        	logger.info("Spliting {} instances into {} ({}) training instances and {} ({}) testing instances.", 
        		new Object[] {instances.size(), trainData.size(), trainingFraction, testData.size(), 1.0f-trainingFraction});
        } else {
        	logger.info("Doing cross validation with {} folds.", nfolds);
        }
        
        logger.info("Random Seed for split: {}", randomSeed);
		logger.info("Number of features in training data: {}", featurePipeline.getDataAlphabet().size());
		logger.info("Initiating training with {} threads.", nthreads);
		logger.info("Gaussian variance: {}", gaussianVariance);
		logger.info("Model in order {}.", modelOrder[0]);
		logger.info("Training model until convergence or after reaching {} iterations", iterations);
		logger.info("Evaluating period in every {} iterations.", evalPeriod);
		
		/*
		 *  initialize mallet CRF
		 *  Sending only the alphabet of the training data is better
		 *  than sending the alphhabet of the whole pipe, preventing 
		 *  from not seriazable pipes 
		 */
	
		//crfmodel = new CRF(featurePipeline, null);
		crfmodel = new CRF(trainData.getDataAlphabet(), trainData.getTargetAlphabet());
		
        String startName =
                crfmodel.addOrderNStates(trainData, modelOrder, null,
                DEFAULT_LABEL, Pattern.compile(FORBIDDEN_PATTERN), null,
                true);
        
        for (int i = 0; i < crfmodel.numStates(); i++) {
            crfmodel.getState(i).setInitialWeight(Transducer.IMPOSSIBLE_WEIGHT);
        }
        
        crfmodel.getState(startName).setInitialWeight(0.0);
        
		/* here is where the actual training happens */
		//TODO: need to check this parameters of initial weights
		CRFTrainerByThreadedLabelLikelihood trainer = new CRFTrainerByThreadedLabelLikelihood(crfmodel, nthreads);
        trainer.setGaussianPriorVariance(gaussianVariance);
        
        // some-dense
        trainer.setUseSparseWeights(true);
        trainer.setUseSomeUnsupportedTrick(true);
        
        /* specify which evaluators to use
         * 
         */
        evals = new TransducerEvaluator [] { 
        		new MultiSegmentationEvaluator(new InstanceList[]{trainData, testData}, new String[]{"Training", "Testing"},
        		START_TAGS, CONTINUE_TAGS),
        		new PerClassAccuracyEvaluator(new InstanceList[]{trainData, testData}, new String[]{"Training", "Testing"}),
        };
        
        
        boolean converged;
        for (int i = 1; i <= iterations; i++) {
        	long start = System.currentTimeMillis();
            converged = trainer.train(trainData, 1);
            long end = System.currentTimeMillis();
            logger.info(((end-start)/1000.0f)+" seconds");
            
            if (evalPeriod!=0 && i % evalPeriod == 0) {
            	logger.info("Number parameters: "+crfmodel.getParameters().getNumFactors());

            	logger.info("Evaluating on iteration {}:", i);
            	for (int j=0;j<evals.length;j++)
            		evals[j].evaluate(trainer);
            }
            
            if (converged) 
                break;
        }
        
        trainer.shutdown(); 
	}
	
	public void testModel(String testDataFilename, String outputFilename) throws IOException {
		logger.info("Starting test phase!");
		InstanceList testData = this.loadInstances(testDataFilename);
		for (int i = 0; i < testData.size(); i++) {
		      Sequence input = (Sequence)testData.get(i).getData();
		      Sequence output = crfmodel.transduce(input);
		      
		      boolean error = false;
              if (output.size() != input.size())
                      error = true;
              
              if (!error) {
                  for (int j = 0; j < input.size(); j++) {
                      StringBuffer buf = new StringBuffer();
                      buf.append(output.get(j).toString()).append(" ");
                      
                      System.out.println(buf.toString());
                  }
                  System.out.println();
              }
		}
	}
	
	public void saveModel(String modelFileName) throws Exception {
	    // Implement own 
		if(crfmodel == null) 
	    	throw new RuntimeException("Model not trained. Nothing to save");
	    
	    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(modelFileName));
	    oos.writeObject(crfmodel);
	    oos.close();
	}
	
	/* 
	 * Private methods 
	 */
	
	private void initilizePipes() {
		String CAPS = "[\\p{Lu}]";
	    String LOW = "[\\p{Ll}]";
	    String ALPHA = "[\\p{Lu}\\p{Ll}]";
	    String ALPHANUM = "[\\p{Lu}\\p{Ll}\\p{Nd}]";
	    String PUNT = "[,\\.;:?!()]";
	    String QUOTE = "[\"`']";
	    
	    // offset conjunctions
	    int [][] conjunctions = new int[10][];
	    conjunctions[0] = new int[]{-1};
        conjunctions[1] = new int[]{1};
        conjunctions[2] = new int[]{-2, -1};
        conjunctions[3] = new int[]{1, 2};
        conjunctions[4] = new int[]{-2};
        conjunctions[5] = new int[]{2};
        conjunctions[6] = new int[]{-3};
        conjunctions[7] = new int[]{3};
        conjunctions[8] = new int[]{-3, -2, -1};
        conjunctions[9] = new int[]{1, 2, 3};
	    
		
	    featurePipeline = new SerialPipes(new Pipe [] { 

	    	// POSTagger needs to be before token sequence
	    	new POSTagger("POS="),
	        //Sentence2TokenSequence pipe
	        new SimpleTaggerSentence2TokenSequence(),
	        new TokenFirstPosition("FIRSTTOKEN"),
	
	        // Caps features
	        new RegexMatches("CAPITALIZED", Pattern.compile(CAPS + LOW + "*")),
	        new RegexMatches("INITCAP", Pattern.compile(CAPS + ".*")),
	        new RegexMatches("ALLCAPS", Pattern.compile(CAPS + "+")),
	        new RegexMatches("MIXEDCAPS", Pattern.compile("[A-Z][a-z]+[A-Z][A-Za-z]*")),
	
	        // Number features
	        new RegexMatches("CONTAINSDIGITS", Pattern.compile(".*[0-9].*")),
	        new RegexMatches("ALLDIGITS", Pattern.compile("[0-9]+")),
	        new RegexMatches("NUMERICAL", Pattern.compile("[-0-9]+[\\.,]+[0-9\\.,]+")),
	        new RegexMatches("STARTSNUMBER", Pattern.compile("^[0-9].*")),
	
	        // Morphologic features
	        new RegexMatches("MULTIDOTS", Pattern.compile("\\.\\.+")),
	        new RegexMatches("HYPHENATED", Pattern.compile(".*\\-.*")),
	        new RegexMatches("DOLLARSIGN", Pattern.compile(".*\\$.*")),
	        new RegexMatches("ENDSINDOT", Pattern.compile("[^\\.]+.*?\\.")),
	        new RegexMatches("CONTAINSDASH", Pattern.compile(ALPHANUM + "+-" + ALPHANUM + "*")),
	        new RegexMatches("ACRO", Pattern.compile("[A-Z][A-Z\\.]*\\.[A-Z\\.]*")),
	        new RegexMatches("LONELYINITIAL", Pattern.compile(CAPS + "\\.")),
	        new RegexMatches("SINGLECHAR", Pattern.compile(ALPHA)),
	        new RegexMatches("CAPLETTER", Pattern.compile("[A-Z]")),
	        new RegexMatches("PUNC", Pattern.compile(PUNT)),
	        new RegexMatches("ENDPUNC", Pattern.compile("[?!\\.]")),
	        new RegexMatches("QUOTE", Pattern.compile(QUOTE)),
	
	        // My features
	        new RegexMatches("APOSTROFE", Pattern.compile("[^']'.*")),
	        new RegexMatches("YEAR", Pattern.compile("[12][0-9][0-9][0-9]")),
	        new RegexMatches("EMAIL", Pattern.compile("[a-zA-Z0-9\\._%+-]+@[a-zA-Z0-9\\.-]+(\\.[a-zA-Z]{2,4})?")),
	
	        new TokenSequenceLowercase(),
	
	        /* Make the word a feature. */
	        new TokenText("WORD="),
	
	       /* new TokenTextCharSuffix("SUFFIX2=",2),
	        new TokenTextCharSuffix("SUFFIX3=",3),
	        new TokenTextCharSuffix("SUFFIX4=",4),
	        new TokenTextCharPrefix("PREFIX2=",2),
	        new TokenTextCharPrefix("PREFIX3=",3),
	        new TokenTextCharPrefix("PREFIX4=",4),
	        new TokenTextCharNGrams ("CHARNGRAM=", new int[] {2,3,4}),
	        */
	        /* FeatureInWindow features. */
	        //new FeaturesInWindow("WINDOW=",-1,1, Pattern.compile("WORD=.*|SUFFIX.*|PREFIX.*|POS=.*|[A-Z]+"),true),
	          	        
	        new OffsetConjunctions(true,Pattern.compile("WORD=.*|POS=.*|[A-Z]+"),
	              conjunctions),
	              
	        new TokenSequence2FeatureVectorSequence(),
	    });
	    
	    featurePipeline.getTargetAlphabet().lookupIndex(DEFAULT_LABEL);
    }
	
	private InstanceList loadInstances(String filename) throws IOException {
		InputStream f = new FileInputStream(filename);
		if (filename.endsWith(".gz"))
			f = new GZIPInputStream(f);

		InstanceList instances = new InstanceList(this.featurePipeline);
		instances.addThruPipe(new LineGroupIterator(new BufferedReader(new InputStreamReader(f)), Pattern.compile("^\\s*$"), true));
		 
		return instances;
	}
	
	/* 
	 * Getters and Setters
	 * 
	 */
	
	public double getTrainingFraction() {
		return trainingFraction;
	}

	public void setTrainingFraction(double trainingFraction) {
		this.trainingFraction = trainingFraction;
	}

	public double getGaussianVariance() {
		return gaussianVariance;
	}

	public void setGaussianVariance(double gaussianVariance) {
		this.gaussianVariance = gaussianVariance;
	}

	public int getRandomSeed() {
		return randomSeed;
	}

	public void setRandomSeed(int randomSeed) {
		this.randomSeed = randomSeed;
	}

	public int getIterations() {
		return iterations;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	public int getEvalPeriod() {
		return evalPeriod;
	}

	public void setEvalPeriod(int evalPeriod) {
		this.evalPeriod = evalPeriod;
	}

	public int getModelOrder() {
		return modelOrder[0];
	}

	public void setModelOrder(int order) {
		this.modelOrder = new int[]{order};
	}
	
	public int getNFolds() {
		return nfolds;
	}

	public void setNFolds(int n) {
		this.nfolds = n;
	}

	 
}
