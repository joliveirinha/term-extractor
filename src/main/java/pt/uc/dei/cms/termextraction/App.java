package pt.uc.dei.cms.termextraction;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * App class. This is where all the command line parsing
 * happens.
 *
 */
public class App 
{
	private Logger logger = LoggerFactory.getLogger(App.class);
	
	private static final String APP_NAME = "TermExtraction";
	
	private Options options;
	
	// Constructor
	private App() {
		logger.info("Application started!");
	}
	
	/*
	 * @param args Command line arguments
	 * Validate all command line arguments and parse them 
	 * to the correct strucutre
	 * 
	 * @return Parsed command line arguments
	 */
	private CommandLine parseOptions(String args[]) {
		
		// define all possible options
		options = new Options();
		
		options.addOption("t", "train", false, "Train model");
		options.addOption("o", "output", true, "output file for label results");
		options.addOption("h", "help", false, "print this message");
		options.addOption("n", "nfolds", true, "number of folds for a k-fold cross validation");
		options.addOption("e", "eval-period", true, "eval period in iterations DEFAULT=0");
		options.addOption("i", "iterations", true, "number of iterations. DEFAULT=INT_MAX");
		options.addOption("tfraction", true, "fraction of training data from file. DEFAULT=0.9");
		options.addOption("rseed", true, "random seed for data split. DEFAULT=0");
		options.addOption("order", true, "order of the CRF model. DEFAULT=1");
		options.addOption("nthreads", true, "number of threads to train the model. DEFAULT=2");
		options.addOption("gaussian", true, "gaussian variance used in the model. DEFAULT=10.0");
		
		// the option for the model file
		Option modelOpt = new Option("m", "model-file", true, "model file");
		modelOpt.setRequired(true);
		options.addOption(modelOpt);
		
		CommandLineParser parser = new PosixParser();
		try {
			CommandLine line = parser.parse(options, args);
			
			if (line.hasOption('h') || line.getArgList().size()!=1) {
				printUsage();
				System.exit(0);
			}
		
			return line;
		} catch (ParseException e) {
			printUsage();
			System.exit(1);
		}
		
		return null;
	}
	
	private void printUsage() {
		HelpFormatter formatter = new HelpFormatter();
		
		formatter.printHelp(APP_NAME + " [OPTIONS] [FILE]", options);
	}
	
	public void run(String args[]) throws Exception {
		TermExtractor te;
		
		CommandLine line = parseOptions(args);
		
		String inFilename = (String) line.getArgList().get(0);
		logger.debug("The input filename is {}", inFilename);
		
		String outFilename = null;
		if (line.hasOption("output"))
			outFilename = line.getOptionValue("output");
		logger.debug("The output filename is {}", outFilename);
		
		if (line.hasOption("train")) {
			te = new TermExtractor();
		} else { 
			te = TermExtractor.loadModel(line.getOptionValue("model-file"));
		}
		
		// set the special options to the TermExtractor
		te.setEvalPeriod(Integer.parseInt(line.getOptionValue(("eval-period"),Integer.toString(TermExtractor.DEFAULT_EVAL_PERIOD))));
		te.setNFolds(Integer.parseInt(line.getOptionValue("nfolds",Integer.toString(TermExtractor.DEFAULT_NFOLDS))));
		te.setIterations(Integer.parseInt(line.getOptionValue("iterations",Integer.toString(TermExtractor.DEFAULT_ITERATIONS))));
		te.setTrainingFraction(Double.parseDouble(line.getOptionValue("tfraction",Double.toString(TermExtractor.DEFAULT_TRAINING_FRACTION))));
		te.setRandomSeed(Integer.parseInt(line.getOptionValue("rseed",Integer.toString(TermExtractor.DEFAULT_RANDOM_SEED))));
		te.setGaussianVariance(Double.parseDouble(line.getOptionValue("gaussian",Double.toString(TermExtractor.DEFAULT_GUASSIAN_VARIANCE))));
		te.setModelOrder(Integer.parseInt(line.getOptionValue("rseed",Integer.toString(TermExtractor.DEFAULT_MODEL_ORDER))));
		int nthreads = Integer.parseInt(line.getOptionValue("nthreads",Integer.toString(TermExtractor.DEFAULT_NUM_THREADS)));
		
		
		// do weed need to train an model'
		if (line.hasOption("train")) {
			te.trainModel(inFilename, nthreads);
			te.saveModel(line.getOptionValue("model-file"));
		} else {
			te.testModel(inFilename, outFilename);
		}
	}
	
	/* Singleton stuff */
	private static class ApplicationHolder { 
	     public static final App INSTANCE = new App();
	}
	
	public static App getInstance() {
		return ApplicationHolder.INSTANCE;
	}
	
	
	/* Main function */
    public static void main( String[] args )
    {
    	App application = App.getInstance();
    	try {
			application.run(args);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
