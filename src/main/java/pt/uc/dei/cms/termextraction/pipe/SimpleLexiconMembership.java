/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.uc.dei.cms.termextraction.pipe;

import cc.mallet.pipe.Pipe;
import cc.mallet.types.Instance;
import cc.mallet.types.Token;
import cc.mallet.types.TokenSequence;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.Serializable;

/**
 *
 * @author fmpr
 */
public class SimpleLexiconMembership extends Pipe implements Serializable
{
	String name;
	gnu.trove.THashSet lexicon;
	boolean ignoreCase;

	public SimpleLexiconMembership (String name, String[] lexicon, boolean ignoreCase)
	{
		this.name = name;
		this.lexicon = new gnu.trove.THashSet ();
		this.ignoreCase = ignoreCase;
                for (String word : lexicon) {
                    this.lexicon.add (ignoreCase ? word.toLowerCase() : word);
                }
		if (this.lexicon.size() == 0)
			throw new IllegalArgumentException ("Empty lexicon");
	}

	public Instance pipe (Instance carrier)
	{
		TokenSequence ts = (TokenSequence) carrier.getData();
		for (int i = 0; i < ts.size(); i++) {
			Token t = ts.get(i);
			String s = t.getText();
			String conS=s;
			//dealing with ([a-z]+), ([a-z]+, [a-z]+), [a-z]+.
			if(conS.startsWith("("))
				conS = conS.substring(1);
			if(conS.endsWith(")") || conS.endsWith("."))
				conS = conS.substring(0, conS.length()-1);
			if (lexicon.contains (ignoreCase ? s.toLowerCase() : s))
				t.setFeatureValue (name, 1.0);
			if(conS.compareTo(s) != 0) {
				if (lexicon.contains (ignoreCase ? conS.toLowerCase() : conS))
					t.setFeatureValue (name, 1.0);
			}
		}
		return carrier;
	}

	// Serialization

	private static final long serialVersionUID = 1;
	private static final int CURRENT_SERIAL_VERSION = 0;

	private void writeObject (ObjectOutputStream out) throws IOException {
		out.writeInt (CURRENT_SERIAL_VERSION);
		out.writeObject (name);
		out.writeObject (lexicon);
		out.writeBoolean (ignoreCase);
	}

	private void readObject (ObjectInputStream in) throws IOException, ClassNotFoundException {
		int version = in.readInt ();
		this.name = (String) in.readObject();
		this.lexicon = (gnu.trove.THashSet) in.readObject();
		this.ignoreCase = in.readBoolean();
	}
}
