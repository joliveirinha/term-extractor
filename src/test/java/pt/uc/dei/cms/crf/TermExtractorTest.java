package pt.uc.dei.cms.crf;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class TermExtractorTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TermExtractorTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( TermExtractorTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testTermExtractor()
    {
        assertTrue( true );
    }
}
