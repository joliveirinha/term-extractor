import sys, re
import MySQLdb

db = MySQLdb.connect(host="kenobi", user="fmpr", passwd="amilab#2010!", db="crf")
cursor = db.cursor()

cursor.execute("SELECT url, annotated_text FROM training_data where ip is not null order by rand()")
#results = cursor.fetchmany(1)
results = cursor.fetchall()

sentences = list()
for result in results:
    url = result[0]
    abstract = result[1]
    for line in abstract.split('\n'):
        #line preprocessing
        line = line.replace('[', ' [ ')
        line = line.replace(']', ' ] ')
        line = line.replace('(', ' ( ')
        line = line.replace(')', ' ) ')
        line = line.replace('.', ' . ')
        line = line.replace(',', ' , ')
        line = re.sub('\\s+', ' ', line)
#       print line
        
        #line bracket parsing
        curr_word = ""
        open_brackets = 0
        
        result = str()
        for c in line:
            if c == '[':
                open_brackets += 1
                first=False
            elif c == ']':
                open_brackets -= 1
            elif c == ' ':
                if len(curr_word) != 0:
                    if open_brackets == 0:
                        result += curr_word + " O\n"
                    elif open_brackets > 0:
                        if first==False:
                            result += curr_word + " I-KEYWORD\n"
                            first=True
                        else:
                            result+= curr_word + " C-KEYWORD\n"
                    else:
                        raise Exception('Unbalanced square brackets in the annotated text for the article: ' + url) # this should never happen
                    curr_word = ""
            else:
                curr_word += c

        result += "\n"
        sentences.append(result)

nfold = 5
tam = len(sentences)

def ensure_dir(f):
    import os
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(f)

for i in xrange(nfold):
    directory = "it%d" % i
    print directory
    ensure_dir(directory)

    train_filename = "%s/train.txt" % directory
    test_filename = "%s/test.txt" % directory
    
    train_file = open(train_filename, "w")
    test_file = open(test_filename, "w")

    sbound = (tam/nfold) * i
    ebound = sbound + (tam/nfold)

    for i in xrange(0, sbound):
        train_file.write(sentences[i])
    for i in xrange(sbound, ebound):
        test_file.write(sentences[i])
    for i in xrange(ebound, tam):
        train_file.write(sentences[i])

    train_file.close()
    test_file.close()
    

