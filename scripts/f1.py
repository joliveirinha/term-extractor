import sys, re

nfolds = 5
path_to_data_dir = "../data"
fold_dir_prefix = "it"

# global stats
global_conll_precision = 0.0
global_conll_recall = 0.0
global_conll_f1 = 0.0
global_mallet_precision = 0.0
global_mallet_recall = 0.0
global_mallet_f1 = 0.0

for fold in xrange(0, nfolds):
	# read file with ground truth data
	gt_file = open(path_to_data_dir + '/' + fold_dir_prefix + str(fold) + '/test.txt', 'r')
	pred_file = open(path_to_data_dir + '/' + fold_dir_prefix + str(fold) + '/test-output.txt', 'r')
	gt_lines = []
	pred_lines = []
	for gt_line in gt_file:
		gt_line = re.sub('\n', '', gt_line)
		gt_lines.append(gt_line)
	for pred_line in pred_file:
		pred_line = re.sub('\n', '', pred_line)
		pred_lines.append(pred_line)
	pred_line_index = 0

	# conll style
	global_results = {'tp':0.0, 'fp':0.0, 'tn':0.0, 'fn':0.0}
	matchCount = 0
	tokenCount = 0
	#per_class_results = {'PER':{'tp':0.0, 'fp':0.0, 'tn':0.0, 'fn':0.0}, 'ORG':{'tp':0.0, 'fp':0.0, 'tn':0.0, 'fn':0.0}, 'LOC':{'tp':0.0, 'fp':0.0, 'tn':0.0, 'fn':0.0}, 'MISC':{'tp':0.0, 'fp':0.0, 'tn':0.0, 'fn':0.0}, 'O':{'tp':0.0, 'fp':0.0, 'tn':0.0, 'fn':0.0}}

	# mallet style
	numCorrectTokens = {}
	numPredTokens = {}
	numTrueTokens = {}

	for gt_line in gt_lines:		
		gt_splited = re.split('\\s+', gt_line)
		if len(gt_splited) <= 1:
			pred_line_index += 1
			continue
		#print "GT_line: " + gt_line
		tokenCount += 1
		gt_token = gt_splited[0];
		gt_label = gt_splited[1];
		#print "GT: " + gt_token + " -> " + gt_label
		
		pred_line = pred_lines[pred_line_index]
		pred_line_index += 1
		#print "PRED_line: " + pred_line
		pred_splited = re.split('\\s+', pred_line)
		token = pred_splited[0]
		label = pred_splited[1]
		#print "PRED: " + token + " -> " + label

		if gt_token != token:
			# lines are out of sync
			offset = 0
			while gt_token != token and offset < 2:
				offset += 1
				
				# try going back
				tmp_line = pred_lines[pred_line_index - offset]
				tmp_line_splited = re.split('\\s+', tmp_line)
				if len(tmp_line_splited) >= 1:
					tmp_token = tmp_line_splited[0]
					tmp_label = tmp_line_splited[1]
					if gt_token == tmp_token:
						# set next index
						pred_line_index = pred_line_index - offset + 1 
						token = tmp_token
						label = tmp_label
						break
				
				# try going forward
				tmp_line = pred_lines[pred_line_index + offset]
				tmp_line_splited = re.split('\\s+', tmp_line)
				if len(tmp_line_splited) >= 1:
					tmp_token = tmp_line_splited[0]
					tmp_label = tmp_line_splited[1]
					if gt_token == tmp_token:
						# set next index
						pred_line_index = pred_line_index - offset + 1
						token = tmp_token
						label = tmp_label
						break
			if gt_token != token:
				raise Exception('Out of sync')

		# this was for harem's multiple token classifications
		match = False
		trueOutput = gt_label
		predOutput = label
		for possible_label in gt_label.split('|'):
			trueOutput = possible_label
			if possible_label == label:
				trueOutput = possible_label
				match = True
				break

		# mallet style
		if not numTrueTokens.has_key(trueOutput):
			numTrueTokens[trueOutput] = 0.0
		if not numCorrectTokens.has_key(trueOutput):
			numCorrectTokens[trueOutput] = 0.0
		if not numPredTokens.has_key(predOutput):
			numPredTokens[predOutput] = 0.0
		numTrueTokens[trueOutput] += 1
		numPredTokens[predOutput] += 1
		if trueOutput == predOutput:
			numCorrectTokens[trueOutput] += 1
		
		if trueOutput == predOutput:
			matchCount += 1
		if label != "O":
			#it's a positive
			if trueOutput == predOutput:
				#it's a true positive
				global_results['tp'] += 1.0
			else:
				#it's a false positive
				global_results['fp'] += 1.0
		else:
			#it's a negative
			if trueOutput == predOutput:
				#it's a true negative
				global_results['tn'] += 1.0
			else:
				#it's a false negative
				global_results['fn'] += 1.0

	precision = global_results['tp']/(global_results['tp'] + global_results['fp'])
	recall = global_results['tp']/(global_results['tp'] + global_results['fn'] + global_results['fp']) #conll evaluator also adds false positives to the recall
	f1 = 2.0 * ((precision * recall) / (precision + recall))
		
	# per-fold stats
	# conll style
	print "\tFold " + str(fold) + " Results (CoNLL Style):"
	print "matchCount: " + str(matchCount)
	print "tokenCount: " + str(tokenCount)
	print "true positives: " + str(int(global_results['tp']))
	print "false positives: " + str(int(global_results['fp']))
	print "true negatives: " + str(int(global_results['tn']))
	print "false negatives: " + str(int(global_results['fn']))
	print "accuracy: " + str((1.0*matchCount)/(1.0*tokenCount))
	print "precision: " + str(precision)
	print "recall: " + str(recall)
	print "f1: " + str(f1)
	global_conll_precision += precision
	global_conll_recall += recall
	global_conll_f1 += f1

	# mallet style
	print "\n\tFold " + str(fold) + " Results (Mallet Style):"
	precision_sum = 0.0
	recall_sum = 0.0
	f1_sum = 0.0
	for label in numCorrectTokens.keys():
		try:
			precision = (1.0 * numCorrectTokens[label]) / (1.0 * numPredTokens[label]);
		except ZeroDivisionError:
			precision = 0.0
		try:
			recall = (1.0 * numCorrectTokens[label]) / ( 1.0 * numTrueTokens[label]);
		except ZeroDivisionError:
			recall = 0.0
		try:
			f1 = (2 * precision * recall) / (precision + recall);
		except ZeroDivisionError:
			f1 = 0.0
		precision_sum += precision	
		recall_sum += recall
		f1_sum += f1
		print label + ": precision=" + str(precision) + " recall=" + str(recall) + " f1=" + str(f1)
	print "avg: precision=" + str(precision_sum / len(numCorrectTokens)) + " recall=" + str(recall_sum / len(numCorrectTokens)) + " f1=" + str(f1_sum / len(numCorrectTokens))
	global_mallet_precision += precision_sum / len(numCorrectTokens)
	global_mallet_recall += recall_sum / len(numCorrectTokens)
	global_mallet_f1 += f1_sum / len(numCorrectTokens)
	print
	
# global stats
print "\nGlobal Statistics:"
print "\tCoNLL Style:"
print "\t\tPrecision: " + str(global_conll_precision / nfolds)
print "\t\tRecall: " + str(global_conll_recall / nfolds)
print "\t\tF1: " + str(global_conll_f1 / nfolds)
print "\n\tMallet Style:"
print "\t\tPrecision: " + str(global_mallet_precision / nfolds)
print "\t\tRecall: " + str(global_mallet_recall / nfolds)
print "\t\tF1: " + str(global_mallet_f1 / nfolds)
